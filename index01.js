let express = require('express');
let app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.send("{msg: Metodo GET <b>/</b>}");
});

app.get('/funcionario', (req, res) => {
    let obj = req.query;
    let nome = obj.nome;
    let sobrenome = obj.sobrenome;
    res.send(`{msg: Metodo GET <b>Funcionario:</b> ${nome} ${sobrenome}}`);
});

app.listen(port, () => console.log("Projeto rodando na porta: " + port));